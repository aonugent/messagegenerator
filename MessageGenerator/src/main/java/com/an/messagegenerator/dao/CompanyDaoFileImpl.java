/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.an.messagegenerator.dao;

import com.an.messagegenerator.model.Company;
import com.an.messagegenerator.model.Guest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author aon
 */
public class CompanyDaoFileImpl implements CompanyDao {
    
    private ObjectMapper mapper = new ObjectMapper();
    private Map<Integer, Company> companyList = new HashMap<>();

    @Override
    public Company getCompany(int id) {
        if (companyList.size() == 0) {
            loadCompanyList();
        }

        return companyList.get(id);
    }

    @Override
    public List<Company> getAllCompanies() {
        if (companyList.size() == 0) {
            loadCompanyList();
        }

        return new ArrayList(companyList.values());

    }

    private void loadCompanyList() {
        try {
            List<Company> list = mapper.readValue(new File("Companies.json"), new TypeReference<List<Company>>() {
            });
            for (Company company : list) {
                companyList.put(company.getId(), company);
            }
        } catch (IOException e) {
            System.out.println("Unable to load Companies.json");
        }

    }
    
}
