/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.an.messagegenerator.dao;

import com.an.messagegenerator.model.Guest;
import java.util.List;

/**
 *
 * @author aon
 */
public interface GuestDao {
    
    public Guest getGuest(int id);
    
    public List<Guest> getAllGuests();
    
}
