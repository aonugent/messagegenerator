/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.an.messagegenerator.dao;

import com.an.messagegenerator.model.Company;
import com.an.messagegenerator.model.MessageTemplate;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author aon
 */
public class MessageTemplateDaoFileImpl implements MessageTemplateDao {

    private ObjectMapper mapper = new ObjectMapper();
    private Map<Integer, MessageTemplate> templateList = new HashMap<>();

    @Override
    public List<MessageTemplate> getAllTemplates() throws IOException {
        if (templateList.size() == 0) {
            loadTemplates();
        }

        return new ArrayList(templateList.values());
    }

    @Override
    public MessageTemplate getMessageTemplate(int id) throws IOException {
        if (templateList.size() == 0) {
            loadTemplates();
        }

        return templateList.get(id);
    }

    private void loadTemplates() throws IOException {
        List<MessageTemplate> list = mapper.readValue(new File("Templates.json"), new TypeReference<List<MessageTemplate>>() {});
        for (MessageTemplate template : list) {
            templateList.put(template.getId(), template);
        }
    }

}
