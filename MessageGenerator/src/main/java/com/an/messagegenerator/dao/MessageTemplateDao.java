/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.an.messagegenerator.dao;

import com.an.messagegenerator.model.MessageTemplate;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author aon
 */
public interface MessageTemplateDao {
    
    public List<MessageTemplate> getAllTemplates() throws IOException;
    
    public MessageTemplate getMessageTemplate(int id) throws IOException;
    
}
