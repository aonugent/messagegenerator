/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.an.messagegenerator.dao;

import com.an.messagegenerator.model.Company;
import com.an.messagegenerator.model.Guest;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author aon
 */
public class GuestDaoFileImpl implements GuestDao {

    private ObjectMapper mapper = new ObjectMapper();
    private Map<Integer, Guest> guestList = new HashMap<>();

    @Override
    public Guest getGuest(int id) {
        if (guestList.size() == 0) {
            loadGuestList();
        }

        return guestList.get(id);
    }

    @Override
    public List<Guest> getAllGuests() {
        if (guestList.size() == 0) {
            loadGuestList();
        }

        return new ArrayList(guestList.values());

    }

    private void loadGuestList() {
        try {
            List<Guest> list = mapper.readValue(new File("Guests.json"), new TypeReference<List<Guest>>() {
            });
            for (Guest guest : list) {
                guestList.put(guest.getId(), guest);
            }
        } catch (IOException e) {
            System.out.println("Unable to load Guests.json");
        }

    }

}
