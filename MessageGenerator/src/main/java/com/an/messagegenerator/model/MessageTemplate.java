/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.an.messagegenerator.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author aon
 */
public class MessageTemplate {
    
    private int id;
    private List<String> messageSections = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<String> getMessageSections() {
        return messageSections;
    }

    public void setMessageSections(List<String> messageSections) {
        this.messageSections = messageSections;
    }
    
}
