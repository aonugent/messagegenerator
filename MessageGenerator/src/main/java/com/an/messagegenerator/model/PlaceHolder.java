/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.an.messagegenerator.model;

/**
 *
 * @author aon
 */
public enum PlaceHolder {
    
    GREETING("GREETING"),
    FIRSTNAME("FIRSTNAME"),
    LASTNAME("LASTNAME"),
    COMPANY("COMPANY"),
    ROOMNUMBER("ROOMNUMBER");
    
    private final String name;
    
    PlaceHolder(String name) {
        this.name = name;
    }
    
    public String getPlaceHolderName() {
        return name;
    }
}
