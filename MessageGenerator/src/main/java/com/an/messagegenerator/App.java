/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.an.messagegenerator;

import com.an.messagegenerator.dao.CompanyDao;
import com.an.messagegenerator.dao.CompanyDaoFileImpl;
import com.an.messagegenerator.dao.GuestDao;
import com.an.messagegenerator.dao.GuestDaoFileImpl;
import com.an.messagegenerator.dao.MessageTemplateDao;
import com.an.messagegenerator.dao.MessageTemplateDaoFileImpl;
import com.an.messagegenerator.model.Company;
import com.an.messagegenerator.model.Guest;
import com.an.messagegenerator.model.MessageTemplate;
import com.an.messagegenerator.model.PlaceHolder;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author aon
 */
public class App {

    public static void main(String[] args) throws IOException {

        GuestDao guestDao = new GuestDaoFileImpl();
        CompanyDao companyDao = new CompanyDaoFileImpl();
        MessageTemplateDao messageTemplateDao = new MessageTemplateDaoFileImpl();
        Scanner scanner = new Scanner(System.in);

        int guestSelection = getGuestSelection(guestDao, scanner);
        Guest guest = guestDao.getGuest(guestSelection);

        int companySelection = getCompanySelection(companyDao, scanner);
        Company company = companyDao.getCompany(companySelection);

        int templateSelection = getTemplateSelection(messageTemplateDao, scanner);
        MessageTemplate template = messageTemplateDao.getMessageTemplate(templateSelection);

        String greeting = "";
        int hour = guest.getReservation().getStartTimestamp().getHour();
        if(hour >= 0 && hour < 12) {
            greeting = "Good Morning";
        } else if (hour >= 12 && hour < 17) {
            greeting = "Good Afternoon";
        } else {
            greeting = "Good Evening";
        }
        
        ArrayList<PlaceHolder> placeHolderList = new ArrayList<>(Arrays.asList(PlaceHolder.values()));

        List<String> placeHolders = new ArrayList<>();
        for (PlaceHolder ph : PlaceHolder.values()) {
            placeHolders.add(ph.getPlaceHolderName());
        }
        
        Map<String, String> placeHolderValues = new HashMap<>();
        placeHolderValues.put(placeHolderList.get(0).getPlaceHolderName(), greeting);
        placeHolderValues.put(placeHolderList.get(1).getPlaceHolderName(), guest.getFirstName());
        placeHolderValues.put(placeHolderList.get(2).getPlaceHolderName(), guest.getLastName());
        placeHolderValues.put(placeHolderList.get(3).getPlaceHolderName(), company.getCompany());
        placeHolderValues.put(placeHolderList.get(4).getPlaceHolderName(), Integer.toString(guest.getReservation().getRoomNumber()));

        StringBuilder message = new StringBuilder();

        for (String section : template.getMessageSections()) {
            if (placeHolders.contains(section)) {
                message.append(placeHolderValues.get(section));
            } else {
                message.append(section);
            }
        }

        System.out.println(message.toString());
    }

    private static int getGuestSelection(GuestDao guestDao, Scanner scanner) {
        List<Guest> guestList = guestDao.getAllGuests();
        int selection = 0;

        System.out.println("Select a guest to send a message to from the list below:");
        for (Guest guest : guestList) {
            System.out.println(guest.getId() + " - " + guest.getFirstName() + " " + guest.getLastName());
        }
        System.out.println("\r");

        selection = validateInput(selection, scanner, guestList.size());
        return selection;
    }

    private static int getCompanySelection(CompanyDao companyDao, Scanner scanner) {
        List<Company> companyList = companyDao.getAllCompanies();
        int selection = 0;

        System.out.println("Select a location where the guest is staying:");
        for (Company company : companyList) {
            System.out.println(company.getId() + " - " + company.getCompany());
        }
        System.out.println("\r");

        selection = validateInput(selection, scanner, companyList.size());
        return selection;
    }
    
    private static int getTemplateSelection(MessageTemplateDao messageTemplateDao, Scanner scanner) throws IOException {
        List<MessageTemplate> templates = messageTemplateDao.getAllTemplates();
        int selection = 0;
        
        System.out.println("Select a message template to use:");
        for (MessageTemplate template : templates) {
            System.out.println(template.getId());
        }
        System.out.println("\r");

        selection = validateInput(selection, scanner, templates.size());
        return selection;
    }

    private static int validateInput(int selection, Scanner scanner, int listSize) {
        boolean validSelection = false;
        while (!validSelection) {
            try {
                selection = Integer.parseInt(scanner.nextLine());
                if (selection < 1 || selection > listSize) {
                    System.out.println("Please select an Id from the available list");
                    continue;
                }

            } catch (NumberFormatException e) {
                System.out.println("Please select a valid number");
                continue;
            }
            validSelection = true;
        }
        return selection;
    }

}
